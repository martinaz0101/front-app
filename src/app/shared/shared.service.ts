import { Injectable } from '@angular/core';
import { NbSidebarService, NbToastrService } from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})

export class SharedService 
{

  constructor(private sidebarService: NbSidebarService, private toasterService: NbToastrService) { }
  
  getSidebarService(): NbSidebarService
  {
    return this.sidebarService;
  }
  showToaster(message:string, title:string, config)
  {
    return this.toasterService.show(message, title, config);
  }
  
}
