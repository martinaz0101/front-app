import { NgxIndexedDB } from 'ngx-indexed-db';
import { Subject, from, Observable, of } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { reject } from 'q';
import { type } from 'os';

export class IndxDB {
    private db: NgxIndexedDB = null;
    private dbName: string;
    private dbVersion:number = 1;
    private storesNames:string[] = [];
    private indexes: any[];
    private primaryKeys: any[];

    constructor(dbName: string, storesNamesNew: string[], indexesNew: any[], primaryKeysNew: any[])
    {
        this.dbName = dbName;
        this.db = new NgxIndexedDB(dbName, 1);
        this.storesNames = storesNamesNew;
        this.indexes = indexesNew;
        this.primaryKeys = primaryKeysNew;
    }
    
    public openAndCreateStores()//Devuelve una promise, hay que suscribirse.
    { 
        return this.db.openDatabase(this.dbVersion, 
            event => {
                this.storesNames.forEach((elem, index) => 
                {
                    let result = this.objectStoreManagement(event, this.storesNames[index], this.indexes[index], this.primaryKeys[index]);
                    if (!result)
                        throw new Error('Cant create object store: ' + elem);
                });
            });
    }

    private objectStoreManagement(event: any, storeName: string, indexes: any[], primaryKeyObject: any)
    {
         //When DB is open we do an object store management.
         let objectStoreCreated = this.createObjectStore(event, storeName, primaryKeyObject)
         if(objectStoreCreated != null && typeof objectStoreCreated != undefined)
         {
            this.createIndexes(objectStoreCreated, indexes);
            return true;
         }
         return false;            
    }

    private createIndexes(objectStore, indexes: any[])
    {
        if(indexes.length != 0)
            indexes.forEach(elem => {
                    objectStore.createIndex(elem.indxName, elem.keyName, { unique: elem.unique });
                });
    }

    private createObjectStore(event: any, storeName: string, primaryKeyObject: any)
    {
        let objectStore = null;
        objectStore = event.currentTarget.result.createObjectStore(storeName, { keyPath: primaryKeyObject.primaryKeyName, autoIncrement: primaryKeyObject.autoIncrement });
        if(objectStore != null)
            return objectStore;
        else
            throw new Error('Cant create the object store: '+ storeName);
    }

    public storesExists(): Promise<boolean>//Devuelve una promise, hay que suscribirse.
    { 
        return new Promise(
            resolve => {
                let request = window.indexedDB.open(this.dbName);
                let exists = true;
                request.onupgradeneeded = 
                    _ => 
                    {
                        exists = false;
                        resolve(false);
                    };
                request.onsuccess = 
                    _ => 
                    {
                        if (!exists)
                        {
                            window.indexedDB.deleteDatabase(this.dbName);
                            //this.haveStoreNames(event.currentTarget.result.objectStoreNames)
                            resolve(false);
                        }
                        let storeNamesThatExist = request.result.objectStoreNames;
                        request.result.close();
                        resolve(this.haveStoreNames(storeNamesThatExist));
                    };
            request.onerror = _ => { throw new Error('Cant verify existence of IndexedDB');};
        });
            
    }


    private haveStoreNames(storeNamesThatExist: DOMStringList)
    {
        let result = 0;
        if(storeNamesThatExist != null && storeNamesThatExist.length != 0)
                this.storesNames.forEach(elem => {
                    if(storeNamesThatExist.contains(elem))
                        result++;
                });
        return result == this.storesNames.length;
    }
    
   

    public deleteDB(): Promise<any>
    {
        return new Promise(
            (resolve, reject) => {
                    this.db = null;
                    this.storesNames = null;
                    // let request = window.indexedDB.open('telemetryDB', this.dbVersion + 1);
                    let requestDeletion = window.indexedDB.deleteDatabase('telemetryDB');
                    requestDeletion.onerror = e => { console.log("Error deleting database."); reject(e); }
                    requestDeletion.onblocked = e => { console.log("Error deleting database. It's blocked"); reject(e); }
                    requestDeletion.onsuccess = e => resolve(true)
                    //  requestDeletion.result.onversionchange = ev => {
                    //                                 if (ev.newVersion === null) 
                    //                                 requestDeletion.result.close(); // Manually close our connection to the db
                    //                             };
                                                    
                                            
            }
        );
        // this.db.openDatabase(1);

        // return this.db.deleteDatabase().then(
        //     _ => console.log('Database deleted successfully'),
        //     error => console.log(error));    
        // return new Promise(
        // (resolve, reject) => {
        //     this.db.openDatabase(1)
        //            .catch(e => { console.log(e); reject('Cant open IDB for clear'); });
        //     let deletedCounter = 0;
        //     this.storesNames.forEach(element => 
        //         this.db.clear(element)
        //                .then( _ => deletedCounter++)
        //                .catch(e => { console.log(e); reject('Cant delete object store: ' + element); })    
        //     );
        //     resolve(deletedCounter == this.storesNames.length);
        // });
    }


    public save(storeName: string, objectToSave:any) {

            let index: number = this.storesNames.indexOf(storeName);
            return this.db.add(this.storesNames[index], objectToSave);

    }

    public getDBName() {
            return this.dbName;
    }

    public getByIndex(storeName: string, valueToGet:string, indexToUse:string) {
        let index: number = this.storesNames.indexOf(storeName);
        this.db.openDatabase(1);
        return from(this.db.getByIndex(this.storesNames[index], indexToUse, valueToGet));
    }

    public getByKey(storeName: string, valueToGet: string) 
    {
        let index: number = this.storesNames.indexOf(storeName);
        this.db.openDatabase(1);
        return this.db.getByKey(this.storesNames[index], valueToGet);
    }

    public getAll(storeName: string) 
    {
        let index: number = this.storesNames.indexOf(storeName);
        return this.db.openDatabase(1).then( 
            _ => this.db.getAll(this.storesNames[index])
            );
    }


}