export class PubKey {
    alg: string;
    use: string;
    kty: string;
    n: string;
    e: string;
}

