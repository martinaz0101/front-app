export class Finca {
    id: number;
    nombre: string;
    ruta_datos: string;
    empresa_id: number;
    eliminado: number;

    constructor(id: number, nombre: string, ruta_datos: string, empresa_id: number, eliminado: number) {
        this.id = id;
        this.nombre = nombre;
        this.ruta_datos = ruta_datos;
        this.empresa_id = empresa_id;
        this.eliminado = eliminado;
    }
}
