export class Temperatura {
    temp_min: number;
    temp_prom: number;
    temp_max: number;
    fecha_hora: string;

    constructor(temp_min: number, temp_prom: number, temp_max: number, fecha_hora: string) {
        this.temp_min = temp_min;
        this.temp_prom = temp_prom;
        this.temp_max = temp_max;
        this.fecha_hora = fecha_hora;
    }
}
