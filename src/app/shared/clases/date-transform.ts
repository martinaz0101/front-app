export abstract class DateTransform
{
    public static getDayOfWeek(date: string):string{
        var t = new Date(date);
        var dayMapping: Map<number,string>  = new Map();
        dayMapping.set(0, 'Domingo');
        dayMapping.set(1, 'Lunes');
        dayMapping.set(2, 'Martes');
        dayMapping.set(3, 'Miercoles');
        dayMapping.set(4, 'Jueves');
        dayMapping.set(5, 'Viernes');
        dayMapping.set(6, 'Sabado');
        return dayMapping.get(t.getDay());
    }
    public static getDayOfMonth(date: string):string{
        var t = new Date(date);
        return t.getDate().toString();
    }
    public static getHourAndMinutes(date: string):string{
        var t = new Date(date); var hours:string; var minutes:string;
        if(t.getHours())
            hours = t.getHours().toString();
        else
            hours = '00';
        console.log(hours);
        if(t.getMinutes())
            minutes = t.getMinutes().toString();
        else
            minutes = '00';   
        return hours + ':' + minutes;
    }
}   