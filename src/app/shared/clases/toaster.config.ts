import { NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbGlobalPosition, NbToastStatus } from '@nebular/theme';

export class ToasterConfig {
    preventDuplicates?: boolean = false;
    duration?: number;
    position?: NbGlobalPosition;
    status?: NbToastStatus;
    hasIcon?: boolean = false;
    icon?: string;
    destroyByClick?: boolean = false;
    constructor(preventDuplicates?: boolean, duration?: number, position?: string, status?: string, hasIcon?: boolean, iconName?: string, destroyByClick?: boolean) {
        this.preventDuplicates = preventDuplicates;
        this.duration = duration;
        //Map string to GlobalPosition.
        var positionsMapping: Map<string,NbGlobalPosition>  = new Map();
        positionsMapping.set('top-right', NbGlobalPhysicalPosition.TOP_RIGHT);
        positionsMapping.set('top-left', NbGlobalPhysicalPosition.TOP_LEFT);
        positionsMapping.set('bottom-right', NbGlobalPhysicalPosition.BOTTOM_RIGHT);
        positionsMapping.set('bottom-left', NbGlobalPhysicalPosition.BOTTOM_LEFT);
        positionsMapping.set('top-end', NbGlobalLogicalPosition.TOP_END);
        positionsMapping.set('top-start', NbGlobalLogicalPosition.TOP_START);
        positionsMapping.set('bottom-end', NbGlobalLogicalPosition.BOTTOM_END);
        positionsMapping.set('bottom-start', NbGlobalLogicalPosition.BOTTOM_START);
        this.position = positionsMapping.get(position);
        //Map string to NbToastStatus.
        var statusMapping: Map<string,NbToastStatus>  = new Map();
        statusMapping.set('default', NbToastStatus.DEFAULT);
        statusMapping.set('danger', NbToastStatus.DANGER);
        statusMapping.set('info', NbToastStatus.INFO);
        statusMapping.set('primary', NbToastStatus.PRIMARY);
        statusMapping.set('success', NbToastStatus.SUCCESS);
        statusMapping.set('warning', NbToastStatus.WARNING);
        this.status = statusMapping.get(status);
        this.hasIcon = hasIcon;
        this.icon = iconName;
        this.destroyByClick = destroyByClick;
    }
}
