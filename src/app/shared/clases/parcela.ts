export class Parcela
{
    id: number;
    nombre: string;
    descripcion: string;
    id_elemento: number;
    finca_id: number;
    eliminado: number;

    constructor(id: number, nombre: string, descripcion: string, id_elemento:number, finca_id:number, eliminado: number) { 
        this.id =id;
        this.nombre =nombre;
        this.descripcion =descripcion;
        this.id_elemento=id_elemento;
        this.finca_id = finca_id;
        this.eliminado = eliminado;
    }
}