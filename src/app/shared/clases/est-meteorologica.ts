// export class EstMeteorologica {
//     id: number;
//     fecha_hora: string;
//     temp_ext: number;
//     vel_viento: number;
//     dir_viento: string;
//     hum_int: number;

//     constructor(id: number, fecha_hora: string, temp_ext: number, vel_viento: number, dir_viento: string, hum_int: number) {
//         this.id = id;
//         this.fecha_hora = fecha_hora;
//         this.temp_ext = temp_ext;
//         this.vel_viento = vel_viento;
//         this.dir_viento = dir_viento;
//         this.hum_int = hum_int;
//     }
// }

export class EstMeteorologica {
    id?: number;
    fecha_hora?: string;
    write_date?: string;
    id_estacion?: number;
    temp_ext?: number;
    temp_max?: number;
    temp_min?: number;
    pto_rocio?: number;
    vel_viento?: number;
    dir_viento?: string;
    rec_viento?: number;
    vel_viento_max?: number;
    dir_viento_max?: string;
    sens_term?: number;
    ind_calor?: number;
    ind_THW?: number;
    indice_THSW?: number;
    bar?: number;
    lluvia?: number;
    int_lluvia?: number;
    rad_solar?: number;
    energia_solar?: number;
    max_rad_solar?: number;
    grad_d_calor?: number;
    grad_d_frio?: number;
    temp_int?: number;
    hum_int?: number;
    rocio_int?: number;
    in_cal_int?: number;
    ET?: number;
    hum_suelo1?: number;
    hum_suelo2?: number;
    hum_suelo3?: number;
    temp_suelo1?: number;
    temp_suelo2?: number;
    temp_suelo3?: number;
    hum_hoja_1?: number;
    muest_viento?: number;
    tx_viento?: number;
    recep_iss?: number;
    int_arc?: number;

    constructor(id?: number, fecha_hora?: string, write_date?: string, id_estacion?: number, temp_ext?: number, temp_max?: number, temp_min?: number,
        pto_rocio?: number, vel_viento?: number, dir_viento?: string, rec_viento?: number, vel_viento_max?: number, dir_viento_max?: string,
        sens_term?: number, ind_calor?: number, ind_THW?: number, indice_THSW?: number, bar?: number, lluvia?: number, int_lluvia?: number, rad_solar?: number,
        energia_solar?: number, max_rad_solar?: number, grad_d_calor?: number, grad_d_frio?: number, temp_int?: number, hum_int?: number,
        rocio_int?: number, in_cal_int?: number, ET?: number, hum_suelo1?: number, hum_suelo2?: number, hum_suelo3?: number, temp_suelo1?: number,
        temp_suelo2?: number, temp_suelo3?: number, hum_hoja_1?: number, muest_viento?: number, tx_viento?: number, recep_iss?: number, int_arc?: number) {
        this.id = id;
        this.id_estacion = id_estacion;
        this.temp_ext = this.temp_ext;
        this.temp_max = temp_max;
        this.temp_min = temp_min;
        this.pto_rocio = pto_rocio;
        this.vel_viento = vel_viento;
        this.dir_viento = dir_viento;
        this.rec_viento = rec_viento;
        this.vel_viento = vel_viento;
        this.vel_viento_max = vel_viento_max;
        this.dir_viento_max = dir_viento_max;
        this.sens_term = sens_term;
        this.ind_calor = ind_calor;
        this.ind_THW = ind_THW;
        this.indice_THSW = indice_THSW;
        this.bar = bar;
        this.lluvia = lluvia;
        this.int_lluvia = int_lluvia;
        this.rad_solar = rad_solar;
        this.energia_solar = energia_solar;
        this.max_rad_solar = max_rad_solar;
        this.grad_d_calor = grad_d_calor;
        this.grad_d_frio = grad_d_frio;
        this.temp_int = temp_int;
        this.hum_int = hum_int;
        this.rocio_int = rocio_int;
        this.in_cal_int = in_cal_int;
        this.ET = ET;
        this.hum_suelo1 = hum_suelo1;
        this.hum_suelo2 = hum_suelo2;
        this.hum_suelo3 = hum_suelo3;
        this.temp_suelo1 = temp_suelo1;
        this.temp_suelo2 = temp_suelo2;
        this.temp_suelo3 = temp_suelo3;
        this.hum_hoja_1 = hum_hoja_1;
        this.muest_viento = muest_viento;
        this.tx_viento = tx_viento;
        this.recep_iss = recep_iss;
        this.int_arc = int_arc;
        this.fecha_hora = fecha_hora;
        this.write_date = write_date;
    }
}
