import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  {
    path: 'not-found',
    component: NotfoundComponent,
    pathMatch: 'full'
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
