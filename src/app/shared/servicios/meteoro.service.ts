import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { EstMeteorologica } from '../clases/est-meteorologica';
import { Et0 } from "../clases/et0";
import { Temperatura } from "../../shared/clases/temperatura";
import { ApiURL } from "../clases/api-url";

const httpOptions =
{
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

//const apiUrl = "http://127.0.0.1/tesis-api/est-meteorologicas";
const apiUrl = ApiURL.API_ENDPOINT+"est-meteorologicas";


@Injectable({
  providedIn: 'root'
})
export class MeteoroService {

  constructor(private http: HttpClient) { }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getEstMeteoroRecordsfromID(id: number): Observable<EstMeteorologica[]> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<EstMeteorologica[]>(url).pipe(
      tap(_ => console.log(`fetched records estMeteorologica estacion id=${id}`)),
      catchError(this.handleError)
    );
  }

  getLastRecord(id: number) {
    const url = `${apiUrl}/${id}/last`;
    return this.http.get(url).pipe(
      tap(_ => console.log(`fetched last record from finca id: ${id}`)),
      catchError(this.handleError)
    );
  }

  getEt0LastWeek(id: number): Observable<Et0[]> {
    const url = `${apiUrl}/${id}/et0/last-week`;
    return this.http.get<Et0[]>(url).pipe(
      tap(_ => console.log(`fetched et0 last week records from finca id: ${id}`)),
      map(etArray => {
        etArray = etArray['estMeteorologica'];
        return etArray;
      }),
      catchError(this.handleError)
    )
  }

  getEt0LastBiWeek(id: number): Observable<Et0[]> {
    const url = `${apiUrl}/${id}/et0/last-biweek`;
    return this.http.get<Et0[]>(url).pipe(
      tap(_ => console.log(`fetched et0 last biweek records from finca id: ${id}`)),
      map(etArray => {
        etArray = etArray['estMeteorologica'];
        return etArray;
      }),
      catchError(this.handleError)
    )
  }

  getEt0LastMonth(id: number): Observable<Et0[]> {
    const url = `${apiUrl}/${id}/et0/last-month`;
    return this.http.get<Et0[]>(url).pipe(
      tap(_ => console.log(`fetched et0 last month records from finca id: ${id}`)),
      map(etArray => {
        etArray = etArray['estMeteorologica'];
        return etArray;
      }),
      catchError(this.handleError)
    )
  }

  getTempLastWeek(id: number): Observable<Temperatura[]> {
    const url = `${apiUrl}/${id}/temp/last-week`;
    return this.http.get<Temperatura[]>(url).pipe(
      tap(_ => console.log(`fetched temp last week records from finca id: ${id}`)),
      map(tempArray => {
        tempArray = tempArray['estMeteorologica'];
        return tempArray;
      }),
      catchError(this.handleError)
    )
  }

  getTempLastDay(id: number): Observable<EstMeteorologica[]> {
    const url = `${apiUrl}/${id}/temp/last-day`;
    return this.http.get<EstMeteorologica[]>(url).pipe(
      tap(_ => console.log(`fetched temp last day records from finca id: ${id}`)),
      map(tempArray => {
        tempArray = tempArray['estMeteorologica'];
        return tempArray;
      }),
      catchError(this.handleError)
    )
  }
}
