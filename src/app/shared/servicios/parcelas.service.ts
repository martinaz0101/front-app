import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Parcela } from '../clases/parcela';
import { ApiURL } from "../clases/api-url";


const httpOptions =
{
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

 //const apiUrl = "http://127.0.0.1/tesis-api/parcelas";
 const apiUrl = ApiURL.API_ENDPOINT+"parcelas";

@Injectable({
  providedIn: 'root'
})

export class ParcelasService {

  constructor(private http: HttpClient) { }

  private handleError(error){
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //window.alert(errorMessage);
    return throwError(errorMessage);
}

  getParcelas(): Observable<Parcela[]> {
    return this.http.get<Parcela[]>(apiUrl)
      .pipe(
        map(parcelasArray => {
          parcelasArray = parcelasArray['parcelas'];
          return parcelasArray;
        }),
        catchError(this.handleError)
      );
  }

  getParcela(id: number): Observable<Parcela> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Parcela>(url).pipe(
      tap(_ => console.log(`fetched parcela id=${id}`)),
      catchError(this.handleError)
    );
  }

  addParcela(parcela): Observable<Parcela> {
    return this.http.post<Parcela>(apiUrl, parcela, httpOptions).pipe(
      tap((parcela: Parcela) => console.log(`added parcela w/ id=${parcela.id}`)),
      catchError(this.handleError)
    );
  }

  updateParcela(id, parcela): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.patch(url, parcela, httpOptions).pipe(
      tap(_ => console.log(`updated parcela id=${id}`)),
      catchError(this.handleError)
    );
  }

  deleteParcela(id): Observable<Parcela> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Parcela>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted parcela id=${id}`)),
      catchError(this.handleError)
    );
  }
}
