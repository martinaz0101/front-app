import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Finca } from '../clases/finca';
import { ApiURL } from '../clases/api-url';

const httpOptions =
{
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

//const apiUrl = "http://127.0.0.1/tesis-api/fincas";
const apiUrl = ApiURL.API_ENDPOINT+"fincas";

@Injectable({
  providedIn: 'root'
})

export class FincasService {

  constructor(private http: HttpClient) { }

  private handleError(error){
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      //window.alert(errorMessage);
      return throwError(errorMessage);
  }

  getFincas(): Observable<Finca[]> {
    return this.http.get<Finca[]>(apiUrl)
      .pipe(
        map(fincasArray => {
          fincasArray = fincasArray['fincas'];
          return fincasArray;
        }),
        catchError(this.handleError)
      );
  }

  getFinca(id: number): Observable<Finca> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Finca>(url).pipe(
      tap(_ => console.log(`fetched finca id=${id}`)),
      catchError(this.handleError)
    );
  }

  addFinca(finca): Observable<Finca> {
    return this.http.post<Finca>(apiUrl, finca, httpOptions).pipe(
      tap((finca: Finca) => console.log(`added finca w/ id=${finca.id}`)),
      catchError(this.handleError)
    );
  }

  updateFinca(id, finca): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.patch(url, finca, httpOptions).pipe(
      tap(_ => console.log(`updated finca id=${id}`)),
      catchError(this.handleError)
    );
  }

  deleteFinca(id): Observable<Finca> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Finca>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted finca id=${id}`)),
      catchError(this.handleError)
    );
  }
}
