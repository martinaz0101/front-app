import { Injectable } from '@angular/core';
import { ApiURL } from '../clases/api-url';
import { Observable, throwError, Subject, forkJoin, of, from } from 'rxjs';
import { catchError, map, switchMap, flatMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../publicKey';
import * as forge from 'node-forge';
const apiUrl = ApiURL.API_ENDPOINT+"login";
import * as jose from 'node-jose-browserify';
import { IndxDB } from '../clases/indexeddb';
import { PubKey } from '../clases/server-pub-keys';
import { Router } from '@angular/router';


const httpOptions =
{
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({providedIn: 'root'})
  


export class AuthService {

  private indxDB: IndxDB = null;
  constructor(private http: HttpClient, private router: Router) { 
 
    //IndxDB creation
    let storeNames = this.getStoresNamesConfig();
    let indexes = this.getIndexesConfig();
    let primaryKeys = this.getPrimaryKeysConfig();
    this.indxDB = new IndxDB('telemetryDB', storeNames, indexes, primaryKeys);

  }


  publicKey = forge.pki.publicKeyFromPem(environment.publicKey);
  private setPayload(user: string, password: string, PKeyToSign: string, PKeyToEnc: string)
  {
    return forkJoin(this.getIP(), of<string>(this.getUserAgent())).
                 pipe(
                  map(([IP, userAgent]) => JSON.stringify(this.getConfigPayload(user, password, IP, userAgent, PKeyToSign, PKeyToEnc))));
  }


  private getConfigPayload(user: string, password:string, IP: string, userAgent: string, PKeyToSign, PKeyToEnc)
  {
    const now = new Date(); //Current datetime.
    var twoMinLater = new Date();
    twoMinLater.setMinutes(twoMinLater.getMinutes() + 60); //Current datetime plus 2 minutes.
    const now_in_mS_unix = Math.floor( now.getTime() / 1000 );
    const two_min_later_in_mS_unix = Math.floor( twoMinLater.getTime() / 1000 );
    return {
      user: user,
      password: password,
      exp: two_min_later_in_mS_unix,
      nbf: now_in_mS_unix,
      iat: now_in_mS_unix,
      aud: 'servidor-telemetria',
      iss: 'cliente-telemetria',
      ip: IP,
      useragent: userAgent,
      PKeyToSign: PKeyToSign,
      PKeyToEnc: PKeyToEnc
    };
  }
  
  private getIP(): Observable<any>
  {
    const headersIP = { headers:  new HttpHeaders({ 'Content-Type': 'text/html'})};
    return this.http.get<any>('http://api.ipify.org', {responseType: 'text' as 'json', withCredentials: false}).pipe(map(res => res as string))
    ;
  }

  private getUserAgent(): string
  {
    return window.navigator.userAgent;
  }

  public login(user: string, password:string)
  {
    // let payload;
    const algorithmsOfJWKtoSign = 'RS256';
    const useOfJWKtoSign = 'sig';
    const algorithmsOfJWKtoEnc = 'RSA-OAEP-256';
    const useOfJWKtoEnc = 'enc';
    let JWKtoSignPKeyServerAux, JWKtoEncPKeyServerAux, JWKtoSignClientAux, JWKtoEncClientAux;

    return this.getServerPubKeys().pipe(
      switchMap( PKeysServer => forkJoin(jose.JWK.asKey(PKeysServer['JWKtoSign']), jose.JWK.asKey(PKeysServer['JWKtoEnc']))),
      switchMap(([JWKtoSignPKeyServer, JWKtoEncPKeyServer]) => {
                                                                  JWKtoSignPKeyServerAux = JWKtoSignPKeyServer;
                                                                  JWKtoEncPKeyServerAux = JWKtoEncPKeyServer;
                                                                  return forkJoin(this.genJWK(algorithmsOfJWKtoSign, useOfJWKtoSign), this.genJWK(algorithmsOfJWKtoEnc, useOfJWKtoEnc))
                                                                }),                                                                                                              
      switchMap(([JWKtoSignClient, JWKtoEncClient]) => {
                                                        JWKtoSignClientAux = JWKtoSignClient;
                                                        JWKtoEncClientAux = JWKtoEncClient;
                                                        return this.setPayload(user, password, this.getPKeyJWK(JWKtoSignClient), this.getPKeyJWK(JWKtoEncClient));
                                                       }),
      switchMap( payload => this.createJWEfromPayload(payload, JWKtoSignClientAux, this.getPKeyJWK(JWKtoEncPKeyServerAux))),
      switchMap(JWE => this.getJWEfromServer(JWE, JWKtoSignPKeyServerAux, JWKtoEncClientAux)),
      switchMap(([jwsEncoded, jwsDecoded]) => from( this.createJWE(jwsEncoded, JWKtoEncPKeyServerAux).
                                                          then( JWE => this.storePairsAndTokens(JWE, JWKtoSignClientAux, JWKtoEncClientAux, jwsDecoded)))),
      catchError(e => this.handleError(e))
    );

  }

  private storePairsAndTokens(JWE, JWKtoSignClient, JWKtoEncClient, jwsDecoded)
  {
    return this.openIndxDBandCreateStores().then(
                _ => {
                      let requestArray = [];
                      let jweToSaveIndxDB = { name: 'jwe', value: JWE };
                      requestArray.push(this.indxDB.save('kPairStoreToSign', this.getBothKeysJWK(JWKtoSignClient)));
                      requestArray.push(this.indxDB.save('kPairStoreToEnc', this.getBothKeysJWK(JWKtoEncClient)));
                      requestArray.push(this.indxDB.save('jweStore', jweToSaveIndxDB));
                      requestArray.push(this.indxDB.save('jwsUnserialStore', jwsDecoded));
                      return               Promise.all(requestArray).
                                   then( _ => Promise.resolve(true)).
                                  catch(error => console.log(error)); 
                  }, error => console.log(error))
  }

  private getPKeyJWK(JWK: any) //Get from a JWK the public key (PKey)
  {
    let jwk = JWK.toJSON();
    return jwk;
  }

  private getBothKeysJWK(JWK: any) //Get from a JWK the public key and private key (PKey + pKey)
  {
    return JWK.toJSON(true);
  }

  public isLoggedIn(): Promise<boolean>
  {
    return this.indxDB.storesExists();
 }


  private getJWEfromServer(JWE: string, JWKtoSignPKeyServer: any, JWKtoEncClient: any)
  {
    const url = `${apiUrl}`;
    const jweAsObject = {data: JWE}; //Sends a JWE too.
    return this.http.post<any>(url, JSON.stringify(jweAsObject), httpOptions)
    .pipe(
      switchMap(
                  res => {
                      //Login successful if there's a token in the response.
                      if (res['token'])
                        return from(this.decryptTokenAndVerify(res['token'], JWKtoEncClient, JWKtoSignPKeyServer));
                      else 
                          return of(false);}),
      catchError(this.handleError));
  }

  private getIndexesConfig(): any[]
  {
    //kPairStoreToSign for JWK.
    var indexKIDtoSign = { indxName: 'kid', keyName: 'kid', unique: true}
    var indexUseToSign = { indxName: 'use', keyName: 'use', unique: false }
    var indexesKpairStoreToSign = [];  
    indexesKpairStoreToSign.push(indexKIDtoSign, indexUseToSign);
    //kPairStoreToEnc for JWK.
    var indexKIDtoEnc = { indxName: 'kid', keyName: 'kid', unique: true}
    var indexUseToEnc = { indxName: 'uuid', keyName: 'uuid', unique: false }
    var indexesKpairStoreToEnc = [];  
    indexesKpairStoreToEnc.push(indexKIDtoEnc, indexUseToEnc);
    //jweStore for JWS.
    var indexesJWEstore = [];
    var indexesArray = []; 
    //jwsUnserialStore for JWS.
    var indexUUID = { indxName: 'uuid', keyName: 'uuid', unique: true}
    var indexesJWSunserialStore = []; 
    indexesJWSunserialStore.push(indexUUID);
    indexesArray.push(indexesKpairStoreToSign, indexesKpairStoreToEnc, indexesJWEstore, indexesJWSunserialStore);
    return indexesArray;
  }

  private getStoresNamesConfig(): any[]
  { 
    let arrayNames = []; arrayNames.push('kPairStoreToSign', 'kPairStoreToEnc', 'jweStore', 'jwsUnserialStore');
    return arrayNames;
  }

  private getPrimaryKeysConfig(): any[]
  { 
    let primaryKey1 = {primaryKeyName: 'kid', autoIncrement: false}; //kPairStoreToSign
    let primaryKey2 = {primaryKeyName: 'kid', autoIncrement: false};  //kPairStoreToEnc
    let primaryKey3 = {primaryKeyName: 'name', autoIncrement: false}; //jwsStoreSerialized
    let primaryKey4 = {primaryKeyName: 'uuid', autoIncrement: false}; //jwsStoreUnserialized
    let arrayPrimaryKeys = []; arrayPrimaryKeys.push(primaryKey1, primaryKey2, primaryKey3, primaryKey4);
    return arrayPrimaryKeys;
  }

  //Open indxDB that already exists, and creates a object store. If not exists indxDB, error.
  private openIndxDBandCreateStores()
  {
    return this.indxDB.openAndCreateStores();
  }

  public getCurrentJWSunserialized()
  {
    return this.indxDB.getAll('jwsUnserialStore').then(
      jwsArray => jwsArray[0]
    );
  }

  public getCurrentJWE()
  {
    return this.indxDB.getAll('jweStore').then(
      jwsArray => jwsArray[0]['value']
    );
  }


  public getKpairIndxDB(storeNameSuffix: 'enc' | 'sign' | 'sig')
  {
    let storeName;
    if(storeNameSuffix == 'sig')
      storeNameSuffix = 'sign';
    const storeNameSuffixCapital = storeNameSuffix[0].toUpperCase() + storeNameSuffix.substr(1).toLowerCase();
    storeName = 'kPairStoreTo' + storeNameSuffixCapital;
    return this.indxDB.getByIndex(storeName, 'use', storeNameSuffix);
  }

  
  private decryptTokenAndVerify(token, jwkToEnc, jwkToSign)
  {
    //Decrypt with node-js library.
      return jose.JWE.createDecrypt(jwkToEnc).
                              decrypt(token). //Input to decrypt as a string, is JWE serialized (parts are base64URL encoded )
                              then(
                              resultDecryption => 
                              {
                                if(resultDecryption == null || typeof resultDecryption == 'undefined')
                                  throw new Error('Cant decrypt the token');
                                  //Call to verify the sign of the token.
                                return this.verifyToken(resultDecryption, jwkToSign);
                              },  error => {  throw new Error('Cant decrypt the token '  + error.message);  })
                              .catch(e => console.log(e));
                              //Just for not see uncaught promises in console.
  }

  private genJWK(algorithms: string , useOfKey: string)//Generate JWK with portion public and private of the key.
  {
    const keystore = jose.JWK.createKeyStore();
    const nBitSize: number  = 2048;
    const kty: string = 'RSA';
    //Next props are for pKey, not for PKey (publica)
    const props = { alg: algorithms, /*key_ops: useOfKey ,*/use: useOfKey /*, kid: generate kid,*/ };
     
    return keystore.generate(kty, nBitSize, props).then(
      _ => keystore.get()
    , error => {throw new Error('Cant generate JWK '+ error.message)});                 

  }


  private verifyToken(jws, jwk: any)
  {
    if(typeof jws  != 'undefined' || (jws != null)) //Just for validation.
    {
      let decodedDecrypted = new TextDecoder("utf-8").decode(jws.payload); //Uint8Array to string.
      //Verifiy the sign of the token with the library.
      return  jose.JWS.createVerify(jwk).
                verify(decodedDecrypted).
                then(resultJWS => 
                {
                  if(typeof resultJWS  != 'undefined' || resultJWS != null)
                  {
                    var decodedJWS = new TextDecoder("utf-8").decode(resultJWS.payload); //Uint8Array to String.
                    let doubleResultJWS = [];
                    doubleResultJWS[0] = decodedDecrypted;
                    doubleResultJWS[1] = JSON.parse(decodedJWS);
                    return Promise.resolve(doubleResultJWS);
                  }
                  else
                  throw new Error('Token not valid');
                }, error => {throw new Error('Token not valid' + error.message)}).
                catch(e => console.log(e));                 
    }
  }

  public createJWEfromPayload(payload: string, pKeyToSign: any, PKeyToEncrypt: any): Observable<string>
  {
    // let result = new Subject<string>();
    return from(
      this.createJWS(payload, pKeyToSign)
                        .then(jwsSerialized => this.createJWE(jwsSerialized, PKeyToEncrypt))
                       );
    // return result;
  }

  private createJWS(input: string, pKey: any)
  {
    // let result = new Subject<string>();
    const options = {alg: 'RS256', format: 'compact' };
    return Promise.resolve(
          jose.JWS.createSign(options, pKey).
                                update(input).
                                final().
                                catch(e => console.log(e)));
                                // then(jwsSerialized => 
                                // {
                                //   result.next(jwsSerialized);
                                //   result.complete();
                                // });
      // return result;
  }

  public createJWE(jwsSerialized: string, PKey: any)
  {
    // let result = new Subject<string>();
    const contentAlg = "A256CBC-HS512";
    const options = 
    {
        compact: true,
        contentAlg: contentAlg,
        protect: Object.keys(
        {
          "alg": PKey.alg,
          "kid": PKey.kid,
          "enc": contentAlg
        }),
        fields: 
        {
          "alg": PKey.alg,
          "kid": PKey.kid,
          "enc": contentAlg
        }
    };
    return Promise.resolve(jose.JWE.createEncrypt(options, PKey).
                            update(jwsSerialized).
                            final().
                            catch(e => console.log(e)));
    // return result;
  }
  public getServerPubKey(method: string): Observable<any> 
  {
      return this.http.get<any>(ApiURL.API_ENDPOINT + 'keys/'+ method, httpOptions).pipe(
        map(res => {
          return res['JWK'];
        })
      );
  }


  public getServerPubKeys(): Observable<{JWKtoSign: PubKey, JWKtoEnc: PubKey}> 
  {
      return this.http.get<{JWK: {JWKtoSign: PubKey, JWKtoEnc: PubKey}}>(ApiURL.API_ENDPOINT + 'keys/', httpOptions).pipe(
        map(res => {
          return res['JWK'];
        })
      );
  }


  public logout() {
    this.indxDB.deleteDB()
               .catch(e => console.log(e));
    window.location.reload();
  }


  private handleError(error): Observable<never>
  {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) 
      errorMessage = `Error: ${error.error.message}`; // client-side error
    else 
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`; // server-side error
    //window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
