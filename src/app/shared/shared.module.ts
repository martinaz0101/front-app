import { NgModule, ModuleWithProviders, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotfoundComponent } from './notfound/notfound.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


//Todo esto es de Material.
import {
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule, MatIconModule,
  MatButtonModule,  MatCardModule,  MatFormFieldModule
      } from "@angular/material";

//Todo esto es de Nebular
import { NbThemeModule, NbLayoutModule, NbActionsModule, NbMenuModule, 
         NbCardModule, NbUserModule, NbPopoverModule, NbListModule, 
         NbInputModule, NbSidebarModule, NbSidebarService, NbSelectModule, NbToastrModule, NbToastrService } from '@nebular/theme';
import { routing } from './shared-routing.module';
import { SharedService } from './shared.service';
import { AuthService } from './servicios/auth.service';
import { FincasService } from './servicios/fincas.service';
import { MeteoroService } from './servicios/meteoro.service';
import { ParcelasService } from './servicios/parcelas.service';
import { JwtInterceptor } from '../auth/jwt.interceptor';
import { ErrorInterceptor } from '../auth/error.interceptor';



@NgModule({
  declarations: [NotfoundComponent],
  imports: [
    routing,
    CommonModule,
    FormsModule,
    //Todo esto es de Material
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    

    //Todo esto es de Nebular
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule, 
    NbActionsModule,
    NbCardModule, 
    NbMenuModule,
    NbUserModule,
    NbPopoverModule,
    NbListModule,
    NbInputModule,
    NbSidebarModule,
    NbToastrModule,
    NbSelectModule,
  ],
  exports: [
    NotfoundComponent,
    CommonModule,
    FormsModule,
    //Todo esto es de Material
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    
    //Todo esto es de Nebular
    NbThemeModule,
    NbLayoutModule, 
    NbActionsModule,
    NbCardModule, 
    NbMenuModule,
    NbUserModule,
    NbPopoverModule,
    NbListModule,
    NbInputModule,
    NbSidebarModule,
    NbToastrModule,
    NbSelectModule,
  ],
  providers: [
    AuthService,
    FincasService,
    MeteoroService,
    ParcelasService,

  ],
})


export class SharedModule {
       // intended to be imported by AppModule
       static forRoot(): ModuleWithProviders {
        return  {
            ngModule: SharedModule, 
            providers: [
                      { 
                        provide: NbSidebarService, useClass: NbSidebarService
                      },
                      { 
                        provide: NbToastrService, useClass: NbToastrService
                      }
                       ]
                };
    }
    // intended to be imported by FeatureModules
    /*static forChild(): ModuleWithProviders {
        return {
            ngModule: SharedModule, 
            providers: []
        }
    }*/
}
