import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import {sidebarItems, SidebarItem} from './sidebar-items.interface';
import {notificationItems, NotificationItem} from './notification-items.interface';
import {configItems, ConfigItem} from './config-items.interface';
import { NbMenuService } from '@nebular/theme';
import { AuthService } from 'src/app/shared/servicios/auth.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HomeComponent implements OnInit{

  title = 'front-app';
  sidebarItems: SidebarItem[];
  popNotificationItems: NotificationItem[];
  popConfigItems: ConfigItem[];

  constructor(private router: Router, private sharedService: SharedService, private menuService: NbMenuService, private authService: AuthService) 
  { 
    this.router=router;
    this.sidebarItems = sidebarItems;
    this.popNotificationItems = notificationItems;
    this.popConfigItems = configItems;
    this.menuService.onItemClick()
                    .subscribe( menuBag => {
                      if(menuBag.item.title == 'Logout')
                        this.authService.logout();
                    });
  }

  ngOnInit() 
  {
  }

  toggle(): boolean
  {
    this.sharedService.getSidebarService().toggle(true);
    return false;
  }

  isHomeRouteActivated(): boolean //Devuelve verdadero si es la ruta raiz o la del actual componente home.
  {
    if(this.router.url == '/home')
      return true;
    return false;
  }


}
