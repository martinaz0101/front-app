export interface ConfigItem 
{
  title: string;
  link?: string;
  icon: string;
}


export const configItems: ConfigItem[] = [
    {
        title: 'Perfil', //Se refiere a fincas.
        link: 'put link here',
        icon: 'fa fa-user-circle'
      },
      {
        title: 'Logout',
        // link: '/login',
        icon: 'fas fa-power-off'
      }
];