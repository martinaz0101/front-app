export interface NotificationItem 
{
  link: string;
  dateTime: string;
  type: string;
  isViewed: string;
}



export const notificationItems: NotificationItem[] =
[
  {
    link: 'put link here',
    dateTime: "2019-06-06",
    type: "Ocurrencia de helada",
    isViewed: "TRUE"
  },
  {
    link: 'put link here',
    dateTime: "2019-06-07",
    type: "Ocurrencia de viento zonda",
    isViewed: "FALSE"
  }
];