export interface SidebarItem 
{
  title: string;
  link: string;
  icon: string;
}


export const sidebarItems: SidebarItem[] = [
    {
        title: 'Dashboard', //Se refiere a fincas.
        link: 'dashboard',
        icon: 'nb-grid-a'
      },
      {
        title: 'Parcela',
        link: 'parcelas',
        icon: 'nb-location'
      },
      {
        title: 'Clima',
        link: 'clima',
        icon: 'nb-partlysunny'
      },
      {
        title: 'Riego',
        link: 'riego',
        icon: 'nb-drops'
      },
      {
        title: 'Gráficos',
        link: 'graficos',
        icon: 'nb-bar-chart'
      }
];