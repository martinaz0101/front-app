import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ParcelasService } from '../../shared/servicios/parcelas.service';
import { StateService } from '../state.service';

@Component({
  selector: 'app-parcelas',
  templateUrl: './parcelas.component.html',
  styleUrls: ['./parcelas.component.scss']
})

export class ParcelasComponent implements OnInit {
  dataSource;
  displayedColumns: string[] = ['nombre', 'descripcion', 'id_elemento', 'finca_id', 'eliminado'];
  status: StateService;

  constructor(private api: ParcelasService, private cdr: ChangeDetectorRef ) {
    this.status = new StateService (this.cdr, 1);
  }

  ngOnInit() {
   this.getParcelas();
  }

  getParcelas()
  {
    this.api.getParcelas().
    subscribe
    (
      res => {
        this.dataSource = res;
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

}