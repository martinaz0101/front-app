import { Injectable, ChangeDetectorRef } from '@angular/core';

export class StateService {

  private resultsToLoad: number;
  private isloadingResults: boolean = true;
  private thereWasError: boolean = false;
  private numberResults: number; //Numero de resultados a cargar, antes de ocultar spinner.
  private cdrRef: ChangeDetectorRef;
  
  constructor(cdr: ChangeDetectorRef, numberResults: number) {
    this.numberResults = numberResults;
    this.resultsToLoad = numberResults;
    this.cdrRef = cdr;
  }

  loadingResults(): boolean {
    return this.isloadingResults;
  }

  setIsloadingResults(isloadingResults: boolean) {
    this.isloadingResults = isloadingResults;
  }

  error(): boolean {
    return this.thereWasError;
  }

  private resetResultsToLoad() {
    this.resultsToLoad = this.numberResults;
  }

  private resetLoadingResults() {
    this.isloadingResults = true;
  }

  private resetThereWasError() {
    this.thereWasError = false;
  }

  resetStatus()
  {
    this.resetLoadingResults();
    this.resetThereWasError();
    this.resetResultsToLoad();
  }
  resultLoaded() {
    this.resultsToLoad--;
    if (this.resultsToLoad == 0) {
      this.isloadingResults = false;
      this.cdrRef.detectChanges(); //Detect changes to re-render component.
    }
  }

  retry() {
    this.resetLoadingResults();
    this.resetResultsToLoad();
    this.resetThereWasError();
    //this.ngOnInitRef();
    this.cdrRef.detectChanges(); //Detect changes to re-render component.
  }

  onError(err: any) {
    // console.log(err);
    this.thereWasError = true;
    this.cdrRef.detectChanges(); //Detect changes to re-render component.
  }

  changeDirectionColumns(thisAux: any, data: any[], columnName: string, valuesName: string, dataSourceName: string, arrayColumnsName: string) {
    thisAux[arrayColumnsName] = [];
    thisAux[dataSourceName] = [];
    var values: number[] = [];
    data.forEach((x, index) => {
      thisAux[arrayColumnsName][index]=x[columnName]; 
      values[x[columnName]]= x[valuesName];
      // thisAux[arrayColumnsName][index] = x.fecha_hora.split(" ")[0];
      // values[x.fecha_hora.split(" ")[0]] = x.ET;
    });
    thisAux[dataSourceName][0] = values;
  }
}
