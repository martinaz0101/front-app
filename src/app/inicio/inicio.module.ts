import { NgModule } from '@angular/core';
import { ParcelasComponent } from './parcelas/parcelas.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { routing } from './inicio-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ErrorComponent } from './error/error.component';
import { TableComponent } from './table/table.component';
import { GraficosComponent } from './graficos/graficos.component';
import { ClimaComponent } from './clima/clima.component';
import { RiegoComponent } from './riego/riego.component';

import { ChartsModule } from "ng2-charts";

@NgModule({
  declarations: [
    ParcelasComponent,
    DashboardComponent,
    HomeComponent,
    ErrorComponent,
    TableComponent,
    GraficosComponent,
    ClimaComponent,
    RiegoComponent,
  ],
  imports: [
    routing,
    SharedModule,
    ChartsModule,
  ],
  
})

export class InicioModule { }
