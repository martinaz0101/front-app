import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() dataSource;
  @Input() displayedColumns: string[];
  @Input() showHourCol: boolean = false;
  @Input() showDateCol: boolean = false;
  dateColumnName = 'fecha_hora';
  ngOnInit() {
    if(this.showHourCol) this.changeColumnName('hora', 'fecha_hora');
    if(this.showDateCol) this.changeColumnName('fecha', 'fecha_hora');
    console.log
  }

  changeColumnName(newColumnName: string, oldColumnName: string) //Cambia la columna fecha_hora por fecha u hora.
  {
    var index = this.displayedColumns.findIndex( element => { return element == oldColumnName; } );
    this.displayedColumns.push(newColumnName);
    if(index != -1) delete this.displayedColumns[index];
  }
}
