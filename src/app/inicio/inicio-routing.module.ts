import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ParcelasComponent } from './parcelas/parcelas.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GraficosComponent } from "./graficos/graficos.component";
import { ClimaComponent } from "./clima/clima.component";
import { RiegoComponent } from "./riego/riego.component";
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    // canDeactivate:[AuthGuard],
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'parcelas',
        component: ParcelasComponent,
        // canDeactivate:[AuthGuard],
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        // canDeactivate:[AuthGuard],
      },
      {
        path: 'graficos',
        component: GraficosComponent,
        // canDeactivate:[AuthGuard],
      },
      {
        path: 'clima',
        component: ClimaComponent,
        // canDeactivate:[AuthGuard],
      },
      {
        path: 'riego',
        component: RiegoComponent,
        // canDeactivate:[AuthGuard],
      }

    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
