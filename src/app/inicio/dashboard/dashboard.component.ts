import { Component, OnInit, ChangeDetectorRef, Injectable, Inject } from '@angular/core';
import { Finca } from '../../shared/clases/finca';
import { FincasService } from '../../shared/servicios/fincas.service';
import { MeteoroService } from '../../shared/servicios/meteoro.service';
import { StateService } from '../state.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  //Datos de finca
  arrayFincas: Finca[] = [];

  //Datos Estacion meteorologica
  estMeteorologicaDataSource = [];
  estMeteorologicaColumns: string[] = ['temp_ext', 'temp_int', 'hum_ext', 'hum_int', 'vel_viento', 'dir_viento', 'pto_rocio', 'sens_term', 'ind_calor', 'bar', 'rad_solar'];

  //Datos ET0
  et0DataSourceLastWeek = [];
  et0DataSourceLastBiweek = [];
  et0DataSourceLastMonth = [];
  et0ColumnsLastWeek: string[] = ['ET', 'fecha_hora'];
  et0ColumnsLastBiweek: string[] = ['ET', 'fecha_hora'];
  et0ColumnsLastMonth: string[] = ['ET', 'fecha_hora'];
  //FLAGS & VARIABLES
  fincaSelected: number;
  finca: Finca;
  isFincaSelected: boolean = false;
  isArrayFincasFetched: boolean = false;
  status: StateService;
  tableETselected: number; //0: Semanal; 1: Quincena; 2: Mes [Por defecto => Semana]

  constructor(private fincasApi: FincasService, private meteoroApi: MeteoroService, private cdr: ChangeDetectorRef) {
    this.status = new StateService(this.cdr, 2);
  }

  ngOnInit() {
    this.getFincas();
    this.resetTableSelected();
  }

  onFincaSelected(idFinca: number) {
    this.resetDataSources();
    this.resetColumns();
    this.resetTableSelected();
    if (idFinca != -1) {
      this.isFincaSelected = true;
      this.finca = this.getFincaById(idFinca);
      this.status = new StateService(this.cdr, 2);
      this.getLastRecord(idFinca);
      this.getET0LastWeek(idFinca);
      //this.getET0LastWeek(idFinca);
      //this.getET0LastBiWeek(idFinca);
      //this.getET0LastMonth(idFinca);
    }
    else {
      this.isFincaSelected = false;
      // this.status.setIsLoadingResults(false);
      // this.semanaBtnClick();
      this.cdr.detectChanges(); //Detect changes to re-render component.
    }
  }

  getFincas() {
    this.fincasApi.getFincas().subscribe(
      res => {
        this.arrayFincas = res;
        this.isArrayFincasFetched = true;
        this.cdr.detectChanges(); //Detect changes to re-render component.
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  //Obtiene finca por id desde arrayFincas
  getFincaById(id: number) {
    return this.arrayFincas.find(finca => finca.id === id);
  }

  getLastRecord(idFinca: number)  //Trae Ultimo registro EstMeteorologica
  {
    this.meteoroApi.getLastRecord(idFinca - 1).subscribe(
      res => {
        this.estMeteorologicaDataSource = Object.values(res);
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  getET0LastWeek(idFinca: number)  //Trae ET0 de la ultima semana
  {
    this.meteoroApi.getEt0LastWeek(idFinca - 1).subscribe(
      res => {
        this.status.changeDirectionColumns(this, res, 'fecha_hora', 'ET', 'et0DataSourceLastWeek', 'et0ColumnsLastWeek');
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  getET0LastBiWeek(idFinca: number)  //Trae ET0 de la ultima quincena
  {
    this.meteoroApi.getEt0LastBiWeek(idFinca - 1).subscribe(
      res => {
        this.status.changeDirectionColumns(this, res, 'fecha_hora', 'ET', 'et0DataSourceLastBiweek', 'et0ColumnsLastBiweek');
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  getET0LastMonth(idFinca: number)  //Trae ET0 del ultimo mes
  {
    this.meteoroApi.getEt0LastMonth(idFinca - 1).subscribe(
      res => {
        this.status.changeDirectionColumns(this, res, 'fecha_hora', 'ET', 'et0DataSourceLastMonth', 'et0ColumnsLastMonth');
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  resetDataSources() {
    this.et0DataSourceLastWeek = [];
    this.et0DataSourceLastBiweek = [];
    this.et0DataSourceLastMonth = [];
  }
  resetColumns() {
    this.et0ColumnsLastWeek = ['ET', 'fecha_hora'];
    this.et0ColumnsLastBiweek = ['ET', 'fecha_hora'];
    this.et0ColumnsLastMonth = ['ET', 'fecha_hora'];
  }

  resetTableSelected() {
    this.tableETselected = 0;
  }
  //Eventos botones tabla ET
  semanaBtnClick() {
    this.status = new StateService(this.cdr, 1);
    this.getET0LastWeek(this.finca.id);
    this.tableETselected = 0;
  }

  quincenaBtnClick() {
    this.status = new StateService(this.cdr, 1);
    this.getET0LastBiWeek(this.finca.id);
    this.tableETselected = 1;
  }

  mesBtnClick() {
    this.status = new StateService(this.cdr, 1);
    this.getET0LastMonth(this.finca.id);
    this.tableETselected = 2;
  }
}
