import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  @Output() retryBtnClickEvent = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  retryBtnClick(){
    this.retryBtnClickEvent.emit();
  }

}
