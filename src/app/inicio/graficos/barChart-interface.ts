import { ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from "chartjs-plugin-datalabels";

const aspect: any = {
  labelFontColor: 'white',
  xAxesFontColor: 'white',
  xAxesGridLinesColor: 'rgba(255,255,255,0.1)',
  yAxesFontColor: 'white',
  yAxesGridLinesColor: 'rgba(255,255,255,0.1)'
}

export const barChartOptions: ChartOptions = {
  responsive: true,
  legend: {
    labels: { fontColor: aspect.labelFontColor }
  },
  scales: {
    xAxes: [{
      ticks: { fontColor: aspect.xAxesFontColor },
      gridLines: { color: aspect.xAxesGridLinesColor }
    }], 
    yAxes: [{
      ticks: { fontColor: aspect.yAxesFontColor },
      gridLines: { color: aspect.yAxesGridLinesColor }
    }]
  },
  plugins: {
    datalabels: {
      anchor: 'end',
      align: 'end',
    }
  }
};

export class BarChartConfig
{
  public type: ChartType = 'bar';
  public plugins: typeof pluginDataLabels[];
  public options: ChartOptions = barChartOptions;
  public legend: boolean = true;
};