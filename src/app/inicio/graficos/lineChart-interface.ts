import { ChartOptions, ChartType } from 'chart.js';
import * as pluginAnnotations from "chartjs-plugin-annotation";

const aspect: any = {
    labelFontColor: 'white',
    xAxesFontColor: 'white',
    xAxesGridLinesColor: 'rgba(255,255,255,0.1)',
    yAxesFontColor: 'white',
    yAxesGridLinesColor: 'rgba(255,255,255,0.1)'
}

export const lineChartOptions: (ChartOptions & { annotation: any }) = 
{
    responsive: true,
    legend: {
        labels: { fontColor: aspect.labelFontColor }
    },
    scales: {
        xAxes: [{
            ticks: { fontColor: aspect.xAxesFontColor },
            gridLines: { color: aspect.xAxesGridLinesColor }
        }],
        yAxes: [
            {
                id: 'y-axis-0',
                position: 'left',
                ticks: { fontColor: aspect.yAxesFontColor },       
                gridLines: { color:  aspect.yAxesGridLinesColor }
            },
        ]
    },
    annotation: {
        annotations: [
            {
                type: 'line',
                mode: 'vertical',
                scaleID: 'x-axis-0',
                value: 'March',
                borderColor: 'orange',
                borderWidth: 2,
                label: {
                    enabled: true,
                    fontColor: 'orange',
                    content: 'LineAnno'
                }
            },
        ],
    },
};

export class LineChartConfig
{
  public type: ChartType = 'line';
  public plugins: typeof pluginAnnotations[];
  public options: (ChartOptions & { annotation: any }) = lineChartOptions;
  public legend: boolean = true;
};