import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EstMeteorologica } from '../../shared/clases/est-meteorologica';
import { Finca } from "../../shared/clases/finca";
import { Temperatura } from "../../shared/clases/temperatura";
import { FincasService } from "../../shared/servicios/fincas.service";
import { MeteoroService } from "../../shared/servicios/meteoro.service";
import { ChartDataSets } from "chart.js";

import { Label } from "ng2-charts";
import { StateService } from '../state.service';
import { Subject} from 'rxjs';
import { BarChartConfig} from '../graficos/barChart-interface';
import { LineChartConfig} from '../graficos/lineChart-interface';
import { DateTransform } from '../../shared/clases/date-transform';


@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.scss']
})

export class GraficosComponent implements OnInit {

  //Flags & Variables
  isArrayFincasFetched: boolean = false;
  fincaSelected: number;
  isFincaSelected: boolean = false;
  finca: Finca;
  isETdataReady: boolean = false;
  isTDdataReady: boolean = false;
  isTSdataReady: boolean = false;

  //Datos de finca
  arrayFincas: Finca[] = [];

  //Datos ET0
  arrayET0LastWeek: EstMeteorologica[] = [];
  //Datos Temperatura
  arrayTempLastDay: EstMeteorologica[] = [];
  arrayTempLastWeek: Temperatura[];
  status: StateService;

  //Grafico Evapotranspiracion (BARRAS)
  public chartETlabels: Label[] = [];
  public chartETdata: ChartDataSets[] = [];
  isETdataReadyObservable: Subject<Boolean>;
  //Graficos de barras en gral.
  barChartConfig: BarChartConfig = new BarChartConfig();
  //Grafico Temperatura Diaria (LINEA)
  public chartTDlabels: Label[] = [];
  public chartTDdata: ChartDataSets[] = [];
  isTDdataReadyObservable: Subject<Boolean>;
  //Grafico Temperatura Semanal (Linea)
  public chartTSlabels: Label[] = [];
  public chartTSdata: ChartDataSets[] = [];
  isTSdataReadyObservable: Subject<Boolean>;
  //Graficos de línea en gral.
  lineChartConfig: LineChartConfig = new LineChartConfig();
  
  constructor(private meteoroApi: MeteoroService, private fincasApi: FincasService, private cdr: ChangeDetectorRef) {
    this.status = new StateService(this.cdr, 3);
    this.isTSdataReadyObservable = new Subject<Boolean>();
    this.isTDdataReadyObservable = new Subject<Boolean>();
    this.isETdataReadyObservable = new Subject<Boolean>();
   }

  ngOnInit() {
    this.getFincas();
  }

  onFincaSelected(idFinca: number) {
    if (idFinca != -1) {
      this.isFincaSelected = true;
      this.status = new StateService(this.cdr, 3);
      this.finca = this.getFincaById(idFinca);
      //OBTENER INFO
      this.getET0LastWeek(idFinca);
      this.getTempLastDay(idFinca);
      this.getTempLastWeek(idFinca);
      this.initializeGraphicFlags();
    }
    else {
      this.isFincaSelected = false;
      this.isTSdataReady = false;
      this.isTDdataReady = false;
      this.isETdataReady = false;
      this.cdr.detectChanges();
    }
  }

  getFincas() {
    this.fincasApi.getFincas().subscribe(
      res => {
        this.arrayFincas = res;
        this.isArrayFincasFetched = true;
        this.cdr.detectChanges();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  getFincaById(id: number) {
    return this.arrayFincas.find(finca => finca.id === id);
  }

  getET0LastWeek(idFinca: number) {
    this.meteoroApi.getEt0LastWeek(idFinca - 1).subscribe(
      res => {
        this.arrayET0LastWeek = res;
        this.isETdataReadyObservable.next(true);
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  getTempLastDay(idFinca: number) {
    this.meteoroApi.getTempLastDay(idFinca - 1).subscribe(
      res => {
        this.arrayTempLastDay = res;
        this.isTDdataReadyObservable.next(true);
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  getTempLastWeek(idFinca: number) {
    
    this.meteoroApi.getTempLastWeek(idFinca - 1).subscribe(
      res => {
        this.arrayTempLastWeek = res;
        this.isTSdataReadyObservable.next(true);
        this.status.resultLoaded();
      },
      err => {
        this.status.onError(err);
      }
    )
  }

  //Barchart
  drawETGraphic() {
    this.chartETlabels =  this.arrayET0LastWeek.map(x => DateTransform.getDayOfWeek(x.fecha_hora));
    this.chartETdata = [{
      data:   this.arrayET0LastWeek.map(x => x.ET),
      label: 'ET0 Ultimos 7 Dias',
      backgroundColor: '#00d977',
      borderColor: '#00d977',
      borderWidth: 3,
      hoverBackgroundColor: '#006b3b',
      hoverBorderColor: '#006b3b',
    }];
  }

  //Linechart
  drawTDGraphic() {
    this.chartTDlabels = this.arrayTempLastDay.map(x=> DateTransform.getHourAndMinutes(x.fecha_hora));
    this.chartTDdata = [{
      data: this.arrayTempLastDay.map(x=> x.temp_ext),
      label: 'Temperaturas Ultimas Horas (Grados)'
    }];
  }

  //Linechart
  drawTSGraphic() {
    if(this.arrayTempLastWeek.length != 0)
    {
      this.chartTSlabels = this.arrayTempLastWeek.map(x=>DateTransform.getDayOfWeek(x.fecha_hora));
      this.chartTSdata = [{
        data:  this.arrayTempLastWeek.map(x=>x.temp_max),
        label: 'Máximas'
      },
      {
        data:  this.arrayTempLastWeek.map(x=>x.temp_min),
        label: 'Minimas'
      },
      {
        data:  this.arrayTempLastWeek.map(x=>x.temp_prom),
        label: 'Promedio'
      }];
   }
  }

  initializeGraphicFlags()
  {
    this.isETdataReadyObservable.subscribe(
      () => {
        this.drawETGraphic();
        this.isETdataReady = true;
      }
    );

    this.isTSdataReadyObservable.subscribe(
      () => {
        this.drawTSGraphic();
        this.isTSdataReady = true;
      }
    );

    this.isTDdataReadyObservable.subscribe(
      () => {
        this.drawTDGraphic();
        this.isTDdataReady = true;
      }
    );
  }

  //Eventos Graficos de Linea
  private chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  private chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  
}
