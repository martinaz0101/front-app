import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';


export const routes: Routes = [
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
    {
      path: 'login',
      loadChildren: './auth/auth.module#AuthModule',
      // canLoad: [AuthGuard],
      // data: { roles: [] }
    },
    {
      path: 'home',
      loadChildren: './inicio/inicio.module#InicioModule',
      canLoad: [AuthGuard],
      data: { roles: [] }
    },
    {
      path: 'not-found',
      loadChildren: './shared/shared.module#SharedModule',
    },
    {
      path: '**',
      redirectTo: 'not-found'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {useHash: true/*, enableTracing: true*/});