import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/servicios/auth.service';
import { ApiURL } from '../shared/clases/api-url';

@Injectable({
    providedIn: 'root'
  })

export class JwtInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const isLoginRequestURL = request.url.startsWith(ApiURL.API_ENDPOINT + 'login'); //ApiURL ya tiene la barra
        const isKeysRequestURL = request.url.startsWith(ApiURL.API_ENDPOINT + 'keys');
        const isIpifyApiRequestURL = request.url.startsWith('http://api.ipify.org');
        if (!isLoginRequestURL && !isKeysRequestURL && !isIpifyApiRequestURL) {
        // add auth header with jwt if user is logged in and request is to api url
        this.authService.getCurrentJWE().then(
                currentJWE => {
                    request = request.clone({
                        setHeaders: {
                            Authorization: `Bearer ${currentJWE}`
                        }
                    });
            }
        );
        }

        return next.handle(request);
    }
}