import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../shared/servicios/auth.service';
import { ApiURL } from '../shared/clases/api-url';

@Injectable({
    providedIn: 'root'
  })

export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
    {
        const isLoginRequestURL = request.url.startsWith(ApiURL.API_ENDPOINT + 'login'); //ApiURL ya tiene la barra
        const isKeysRequestURL = request.url.startsWith(ApiURL.API_ENDPOINT + 'keys');
        const isIpifyApiRequestURL = request.url.startsWith('http://api.ipify.org');
        if (!isLoginRequestURL && !isKeysRequestURL && !isIpifyApiRequestURL) {
            return next.handle(request).pipe(catchError(err => {
                if ((err.status === 401)) 
                {
                    // auto logout if 401 response returned from api
                    this.authService.logout();
                }
                let errorMessage: string = '';
                if(err.error instanceof ErrorEvent)
                    // const error = err.error.message || err.statusText;
                    errorMessage = 'Error: ' + err.message;
                else
                    errorMessage = 'Error code: ' + err.message + ' Message: ' + err.message;
                console.log(errorMessage);
                return throwError(errorMessage);
            }))
        }
        return next.handle(request);
    }
}