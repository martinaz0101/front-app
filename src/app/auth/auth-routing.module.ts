import { LoginComponent } from './login/login.component'
import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { 
    path: '', 
    component: LoginComponent,
    canActivate: [AuthGuard],
    data: { roles: [] },
  },
  // {
  //   path: 'home',
  //   redirectTo: '/home'
  // }
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
