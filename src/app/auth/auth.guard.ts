import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route, CanDeactivate, CanActivate } from '@angular/router';
import { AuthService } from '../shared/servicios/auth.service';
import { of, Observable, from } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanLoad{
    constructor( private router: Router, private authService: AuthService){ 

    }
       
        
    

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> //canActivate route '/login'
    {
        return from(this.authService.isLoggedIn().then(isLoggedIn =>    {
                                                                            if(isLoggedIn)
                                                                            {   
                                                                                this.router.navigate(['/home']);
                                                                                return false;
                                                                            }
                                                                            return true;
                                                                        })).
               pipe(catchError(  e => { console.log(e); 
                                      return of(false); }
               ));       
    }

    canLoad(route: Route): Observable<boolean>
    {
        return from(
                    this.authService.isLoggedIn().then(
                        isLoggedIn => {
                            if(isLoggedIn)
                                return this.authService.getCurrentJWSunserialized()
                                            .then(
                                                jwsUnencoded => 
                                                    this.isRoleAuthorized(jwsUnencoded['rol'], route.data.roles),
                                                e => {console.log(e); return false;});
                            else
                            {
                                this.router.navigate(['/login']);
                                return false;
                            }
                        }
                    ));
            
    }

    private isRoleAuthorized(roleUser:string, roleRoute): boolean
    {
        if(roleRoute.length == 0)
            return true;
        else
            return roleRoute.indexOf(roleUser) === -1
    }
}