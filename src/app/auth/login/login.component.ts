import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/servicios/auth.service';
import { Router } from '@angular/router';
import { NbGlobalPhysicalPosition } from '@nebular/theme';
import { SharedService } from 'src/app/shared/shared.service';
import { ToasterConfig } from 'src/app/shared/clases/toaster.config';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  inicioSesionForm: FormGroup;
  user: string;
  password: string;
  isLoading = false;


  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router, private sharedService: SharedService, private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.inicioSesionForm = this.fb.group({
      'user': ['', [
        Validators.required,
        Validators.pattern("([a-zA-Z0-9]*)([A-Z]*)([0-9]*)([$-_.+!*'(),]*)"),
        Validators.minLength(5),
        Validators.maxLength(16)]
      ],
      'password': ['', [
        Validators.required,
        //Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.pattern("([a-zA-Z0-9]*)([A-Z]*)([0-9]*)([$-_.+!*'(),]*)"),
        Validators.minLength(8),
        Validators.maxLength(32)]
      ]
    });
  }

  //User
  resetUserStatus(): void {
    if (this.isUserEmpty())
      this.inicioSesionForm.controls['user'].reset();
  }

  isUserInvalid(): boolean {
    return this.inicioSesionForm.controls['user'].dirty && this.inicioSesionForm.controls['user'].invalid;
  }

  isUserInitialStatus(): boolean {
    return this.inicioSesionForm.controls['user'].pristine && this.inicioSesionForm.controls['user'].invalid;
  }

  isUserValid(): boolean {
    return this.inicioSesionForm.controls['user'].valid;
  }

  isUserEmpty(): boolean {
    return this.inicioSesionForm.value.user === '';
  }

  //Password
  resetPassStatus(): void {
    if (this.isPassEmpty())
      this.inicioSesionForm.controls['password'].reset();
  }

  isPassInvalid(): boolean {
    return this.inicioSesionForm.controls['password'].dirty && this.inicioSesionForm.controls['password'].invalid;
  }

  isPassValid(): boolean {
    return this.inicioSesionForm.controls['password'].valid;
  }

  isPassInitialStatus(): boolean {
    return this.inicioSesionForm.controls['password'].pristine && this.inicioSesionForm.controls['password'].invalid;
  }

  isPassEmpty(): boolean {
    return this.inicioSesionForm.value.password === '';
  }

  showToaster(message?: string) {
    var toasterConfig = new ToasterConfig(false, 30000, 'bottom-right', 'danger', true, 'fa fa-ban', true);
    this.sharedService.showToaster(message, 'Error', toasterConfig)
  }
  onSubmit(): void {
    if(this.isPassValid() && this.isUserValid() && !this.isLoading)
    {
      this.auth.login(this.user, this.password).subscribe(
        res => {
          if (res)
            this.router.navigateByUrl('/home');
          this.isLoading = false;
          this.cdr.detectChanges(); //Detect changes to re-render component.
        },
        err => {
          this.showToaster(err);
          this.isLoading = false;
          this.cdr.detectChanges(); //Detect changes to re-render component.
        }
      )
      this.isLoading = true;
      this.cdr.detectChanges(); //Detect changes to re-render component.
    }
    else this.showToaster('Verifica tus datos');
  }
}
