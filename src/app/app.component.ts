import { Component, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { NbMenuModule} from '@nebular/theme';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

@NgModule({

})
export class AppComponent 
{
  
  constructor()
  {

  }

}


